var ListMenuView = ToolbarView.extend({
	render_:function(){
		var self = this;
		var menu_tpl = [
			'<ul class="main-toolbar-wrapper">',
				'<li class="main-toolbar-item select off list-type">样式',
					'<ul>',
						'<li data-type="ol" class="ol">列表</li>',
						'<li data-type="ul" class="ul">列表</li>',
					'</ul>',				
				'</li>',
				this.commonTpl,
			'</ul>',
		].join('\n');
		this.$el.html(menu_tpl);
		return this.$el;
	},
	doSelectActions:function(e,select){
		if(select.is('.list-type')){
			var type = e.target.dataset.type;
			var target = ctpl.get('target');
			var list_ol = target.find('ol');
			var list_ul = target.find('ul');
			var parent = this.opts.parent;
			if(type == 'ul'){
				if(list_ol.length==1){
	  				list_ol.replaceWith($('<ul>'+list_ol.html()+'</ul>'))
	  			}else if(list_ul.length==0){
	  				target.html(parent.moptions()['list'].text);
	  			}
			}else{
				if(list_ul.length==1){
	  				list_ul.replaceWith($('<ol>'+list_ul.html()+'</ol>'))
				}else if(list_ol.length==0){
					var tpl = $(parent.moptions()['list'].text);
	  				target.html($('<ol>'+tpl.html()+'</ol>'));
				}
			}
		}
	}
});
var ListView = TemplateView.extend({
	classPrefix:'ctpl-list',
	updateMenu:function(){
		this.menu = new ListMenuView(this.opts);	
	}
});