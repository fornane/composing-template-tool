var ToolbarView = Backbone.View.extend({
	tagName:'div',
	className:'ctpl-main-toolbar',
	events:{},
	default_events:{
		'tap .main-toolbar-item.select':'toggleSelectStatus',
		'tap .main-toolbar-item.select li':'doSelectAction',
		'tap .main-toolbar-item.ok':'ok',
		'dragstart .main-toolbar-item.move':'moveStart',
		'dragend .main-toolbar-item.move':'moveEnd'
	},
	commonTpl:[
		'<li class="main-toolbar-item select off add_item">添加',
			'<ul>',
				'<li data-type="title">标题</li>',
				'<li data-type="paragraph">段落</li>',
				'<li data-type="image">图片</li>',
				'<li data-type="list">列表</li>',
				'<li data-type="quote">引用</li>',
				'<li data-type="catalog">目录</li>',
			'</ul>',				
		'</li>',
		'<li class="main-toolbar-item move">↕移动</li>',
		'<li class="main-toolbar-item select off more">更多',
			'<ul>',
				'<li data-type="preview">预览</li>',
				'<li data-type="remove">删除</li>',
			'</ul>',				
		'</li>',
		].join('\n'),
	direction:null,
	initialize:function(opt){
		this.opts = opt;
		_.extend(this.events,this.default_events);
	},
	render:function(){
		var self = this;
		var parent = self.opts.parent;
		$('.'+self.className).remove();
		if(_.isFunction(self.render_))
			self.render_();
		self.$el.css({'width':parent.opts.width});
		parent.$el.prepend(self.$el);
		return this.$el;
	},
	ok:function(e){
		var view = ctpl.get('view');
		if(!view)
			return;
		view.stopEdit(ctpl.get('target'));
	},
	remove:function(e){
		var view = ctpl.get('view');
		if(!view)
			return;
		var words = ['确认删除 [',$('<a>'+view.opts.text+'</a>').text().substring(0,20),'...]?'].join(' ');
		if(confirm(words)){
			view.remove();
			ctpl.set({
				'target':undefined,
				'view':undefined
			})
		}
	},
	moveStart:function(e){
        var g = e.gesture;
        var self = $(e.target);
        if(g.direction == 'down'){
        	self.html('↓下移');
        	this.direction = g.direction;
        }else if(g.direction == 'up'){
        	self.html('↑上移');
         	this.direction = g.direction;
       }else{
        	self.html('↕移动');
         	this.direction = null;
       }
		event.preventDefault();
	},
	moveEnd:function(e){
        var self = $(e.target);
		var main = ctpl.get('target');
		if(!main)
			return;
		main = main.parents('.ctpl-menu');
		switch(this.direction){
			case 'up':
				var co = main.prev('.ctpl-menu');
				if(co.length!=0){
					main.insertBefore(co.eq(0));
				}
			break;
			case 'down':
				var co = main.next('.ctpl-menu');
				if(co.length!=0){
					main.insertAfter(co.eq(0));
				}
			break;
		}
       	self.html('↕移动');
       	event.preventDefault();
	},
	toggleSelectStatus:function(e){
		var self = $(e.target);
		self.toggleClass('on off');
	},
	doSelectAction:function(e){
		var target = $(e.target);
		var select = $(e.target).parents('.select');
		if(select.is('.add_item')){
			this.addNewItem(e);
		}if(select.is('.more')){
			var type = target.data('type');
			if(type=='preview'){
				alert();
			}else if(type=='remove'){
				this.remove();
			}
		}else{
			this.doSelectActions(e,select);
		}
		select.toggleClass('on off');
	},
	addNewItem:function(e){
		var self = $(e.target);
		var type = self.data('type');
		if(type){
			var main = this.opts.parent;
			$('.editing').removeClass('.editing');
			main.addView(type,main.moptions()[type]);
		}
	}
});